import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private oauth: OAuthService
  ) { }

  init() {

    from(this.oauth.tryLogin())

    // Popup blocker!!!
    // if(!this.getToken()){
    //   this.login()
    // }
  }

  login() {
    this.oauth.initLoginFlow()
  }

  logout() {
    this.oauth.logOut()
  }

  getToken() {
    return this.oauth.getAccessToken()
  }

}
