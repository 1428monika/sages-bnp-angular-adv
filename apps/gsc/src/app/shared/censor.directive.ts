import { Directive, ElementRef, Input } from '@angular/core';
import { AbstractControl, NgModel, ValidationErrors, Validator, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[bnpCensor]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: CensorDirective,
      multi: true // inject as array elem
    }
  ]
})
export class CensorDirective implements Validator { //AbstractValidatorDirective{

  @Input('bnpCensor')
  badword = ['batman']

  constructor(
    private elem: ElementRef,
    // private badwords: BadWORDSTOKENOrClass
    // private model: NgModel
  ) {
    // console.log('hello', elem, model);

  }

  validate(control: AbstractControl): ValidationErrors | null {

    return this.badword.some(word => String(control.value).includes(word)) ? {
      'censor': true
    } : null

    // return {
    //   required: true,
    //   minlength: true
    // }
  }

}
