import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from "../../../core/model/Playlist";

@Component({
  selector: 'bnp-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist?: Playlist

  @Output() edit = new EventEmitter<string>();

  editClick() {
    if (this.playlist) {
      this.edit.emit(this.playlist.id)
    }
  }

  constructor() { }

  ngOnInit(): void { }
}
