import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { DefaultValueAccessor, NgForm, NgModel } from '@angular/forms';
import { MatFormFieldControl, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { Playlist } from "../../../core/model/Playlist";

NgForm
NgModel
MatInput
DefaultValueAccessor
MatFormFieldControl

@Component({
  selector: 'bnp-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.css'],
  providers: [
    // {
    //   provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
    //   useValue: { appearance: 'standard' }
    // }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistEditorComponent implements OnInit {

  @Input('playlist')
  original?: Playlist

  playlist: Playlist = {
    id: '',
    name: '',
    public: false,
    description: ''
  }
  @Output() cancel = new EventEmitter<string>();
  @Output() save = new EventEmitter<Playlist>();

  constructor() {
    console.log('constructor', this.playlist?.name)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.playlist = this.original || this.playlist
    // Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    // Only detects new references (copies) - Shallow detection
    console.log('ngOnChanges', changes)
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.    
    console.log('ngOnInit', this.playlist?.name)
  }

  @ViewChild('formRef', { read: NgForm })
  formRef?: NgForm


  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    console.log('ngAfterViewInit')
    // ERROR Error: NG01000: 
    // There are no form controls registered with this group yet. If you're using ngModel,
    // you may want to check next tick (e.g. use setTimeout).
    // this.formRef?.setValue(this.playlist)
    setTimeout(() => {
      // this.formRef?.setValue(this.playlist)
      // this.formRef?.control.patchValue(this.playlist)
    })
  }

  ngDoCheck(): void {
    // Called every time that the input properties of a component or a directive are checked. 
    // Use it to extend change detection by performing a custom check.
    console.log('ngDoCheck')
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    console.log('ngOnDestroy')
  }

  cancelClick() {
    this.cancel.emit(this.playlist?.id)
  }

  submit(formRef: NgForm) {
    if (formRef.invalid) return;

    const draft = {
      ...this.playlist,
      ...formRef.value
    }

    this.save.emit(draft)
  }

}
