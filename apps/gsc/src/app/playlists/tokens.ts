import { InjectionToken } from '@angular/core';
import { Playlist } from '../core/model/Playlist';

const tokens = {};
export const INITIAL_PLAYLISTS_DATA = new InjectionToken<Playlist[]>('Initial playlists data');
