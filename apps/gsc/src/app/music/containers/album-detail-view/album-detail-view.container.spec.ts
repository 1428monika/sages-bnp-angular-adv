import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumDetailViewContainer } from './album-detail-view.container';

describe('AlbumDetailViewContainer', () => {
  let component: AlbumDetailViewContainer;
  let fixture: ComponentFixture<AlbumDetailViewContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumDetailViewContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumDetailViewContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
